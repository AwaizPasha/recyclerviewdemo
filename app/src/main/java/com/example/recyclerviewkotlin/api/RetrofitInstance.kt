package com.example.recyclerviewkotlin.api

import com.example.recyclerviewkotlin.model.FruitItem
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.*

object RetrofitInstance {
    // base link
    val link = "http://jsonplaceholder.typicode.com/"
    // retrofit instance creation
    private val retrofit = Retrofit.Builder()
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(link)
            .build()

// this interface has the methods that can be used to call api
    interface PazoApiService {
        @GET("photos")
        fun getPhotos() : Call<List<FruitItem>>

//        @POST("details")
//        fun getDetailsFromApi(id:I):Call<FruitItem>
    }
    object PazoApi {
        // creating a service of type RetrofitInstance
        val retrofitService : PazoApiService by lazy { retrofit.create(
                PazoApiService::class.java) }
    }

}
