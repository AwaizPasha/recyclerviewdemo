package com.example.recyclerviewkotlin.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerviewkotlin.R
import com.example.recyclerviewkotlin.model.FruitItem
import com.squareup.picasso.Picasso

class FruitAdapter(var fruitList:List<FruitItem>): RecyclerView.Adapter<FruitAdapter.FruitViewHolder>() , View.OnClickListener {


    var position_of_recycler_view = 0

    // called when the adapter is created and is used to initialize your ViewHolder(s).
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FruitViewHolder {
        val v = LayoutInflater.from(parent?.context).inflate(R.layout.row_data, parent, false);
        return FruitViewHolder(v);
    }
  // called for each ViewHolder to bind it to the adapter.
    override fun onBindViewHolder(holder: FruitViewHolder, position: Int) {
      position_of_recycler_view = holder.adapterPosition
      Picasso.get().load(fruitList[position].thumbnailUrl).into(holder.thumbnailImage);
      holder.fname.text= fruitList[position].id.toString()
      holder.fdesc.text=fruitList[position].title;
      holder.delete_row.setOnClickListener(this)

    }
// returns the size of collection to dispaly
    override fun getItemCount(): Int {

        println(" fsdffgf ${fruitList.size}")
        return fruitList.size;
    }

    override fun onClick(v: View?) {
      //  fruitList.removeAt(position_of_recycler_view);
        notifyDataSetChanged();

    }



    class FruitViewHolder(private val view: View): RecyclerView.ViewHolder(view) {
        val fname: TextView = view.findViewById(R.id.fname)
        val fdesc: TextView = view.findViewById(R.id.fdesc)
        val thumbnailImage: ImageView = view.findViewById(R.id.thumbnailImage)
        val delete_row : Button = view.findViewById(R.id.delete)


//            view.setOnClickListener {
//                val context: Context = view.getContext()
//                val intent = Intent(context, Description::class.java)
//                context.startActivity(intent)
//
//            }

    }




}
