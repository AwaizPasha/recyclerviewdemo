package com.example.recyclerviewkotlin.model
// class for each fruit Item
class FruitItem(

    val albumId: Int,
    val id: Int,
    val thumbnailUrl: String,
    val title: String,
    val url: String
)