package com.example.recyclerviewkotlin.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.recyclerviewkotlin.model.FruitItem
import com.example.recyclerviewkotlin.repository.getPhotosRepository
import org.json.JSONArray


class getPhotosModel: ViewModel(){


    var  fruitLiveData:LiveData<List<FruitItem>> = MutableLiveData<List<FruitItem>>();

    fun getPhotosParams()  {
         fruitLiveData=getPhotosRepository.getPhotos()
    }

}

