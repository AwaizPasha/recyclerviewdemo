package com.example.recyclerviewkotlin.repository

import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerviewkotlin.R
import com.example.recyclerviewkotlin.adapter
import com.example.recyclerviewkotlin.adapter.FruitAdapter
import com.example.recyclerviewkotlin.api.RetrofitInstance
import com.example.recyclerviewkotlin.model.FruitItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object getPhotosRepository {


    fun getPhotos() : LiveData<List<FruitItem>> {
        var data = MutableLiveData<List<FruitItem>>()
        RetrofitInstance.PazoApi.retrofitService.getPhotos()
                .enqueue(object : Callback<List<FruitItem>> {
                    // fuction if api call fails
                    override fun onFailure(call: Call<List<FruitItem>>, t: Throwable) {

                        data.value= listOf();
                    }
                    // fuction if api receives a response
                    override fun onResponse(call: Call<List<FruitItem>>, response: Response<List<FruitItem>>) {
                        if (response.body() != null)
                            //if there is a body for the response we need to create adapter of the response type and attach it to our recycler view
                            {
                                //hide the progress bar when the data is fetched
//                                findViewById<ProgressBar>(R.id.indeterminateBar).setVisibility(View.INVISIBLE)

                                data.value =  response.body()!!
                            }
                    }
                })
        return data
    }
}