package com.example.recyclerviewkotlin

import android.content.DialogInterface
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerviewkotlin.api.RetrofitInstance
import com.example.recyclerviewkotlin.model.FruitItem
import com.example.recyclerviewkotlin.adapter.FruitAdapter
import com.example.recyclerviewkotlin.viewModel.getPhotosModel
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
val fruitsList = mutableListOf<FruitItem>()
lateinit var adapter: FruitAdapter


class MainActivity : AppCompatActivity() {

    lateinit var viewModel:getPhotosModel

   lateinit var fruitList:MutableList<FruitItem>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val cameraOption : Button = findViewById(R.id.camera)

        // Check for  internet  availability
        val connectivityManager = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        val isConnected: Boolean = activeNetworkInfo?.isConnectedOrConnecting == true
        val recyclerView : RecyclerView = findViewById(R.id.recyclerView)
        //item decorator to separate the items
        recyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        viewModel= ViewModelProvider(this).get(getPhotosModel::class.java);
        viewModel.getPhotosParams();
        fruitList= mutableListOf<FruitItem>()
        viewModel.fruitLiveData.observe(this, Observer {
            adapter = FruitAdapter(it)
            recyclerView.adapter = adapter
        })




//        Log.i("Response" , fruitlist.toString())


        // function to be called if internet is available



            //Calling Api using retrofit which returns a list of object of  Fruit Class

//            RetrofitInstance.PazoApi.retrofitService.getPhotos()
//                .enqueue(object : Callback<List<FruitItem>> {
//                    // fuction if api call fails
//                    override fun onFailure(call: Call<List<FruitItem>>, t: Throwable) {
//                        val toast = Toast.makeText(applicationContext, "ONFAILURE ", Toast.LENGTH_LONG)
//                        toast.show()
//                    }
//                    // fuction if api receives a response
//                    override fun onResponse(call: Call<List<FruitItem>>, response: Response<List<FruitItem>>) {
//                        if (response != null)
//                        // if there is a response
//                        {
//                            if (response.body() != null)
//                            //if there is a body for the response we need to create adapter of the response type and attach it to our recycler view
//                            {
//                                //hide the progress bar when the data is fetched
//                                findViewById<ProgressBar>(R.id.indeterminateBar).setVisibility(View.INVISIBLE)
//                                adapter = FruitAdapter(response.body()!!);
//                                recyclerView.adapter = adapter
//
//                            }
//
//                        }
//                        // if api call is made but no response is received
//                        else {
//                            val toast = Toast.makeText(
//                                applicationContext,
//                                "INVALIDCREDENTIALS ",
//                                Toast.LENGTH_LONG
//                            )
//                            toast.show()
//
//                        }
//                    }
//                })
//
//        }

//        else
//        // if no internet is available display a alert message saying no internet
//        {
//            val builder = AlertDialog.Builder(this)
//            builder.setTitle("No internet Connection")
//            builder.setMessage("Please turn on internet connection to continue")
//            builder.setNegativeButton("close", DialogInterface.OnClickListener { dialog, which -> finishAffinity() })
//            val alertDialog: AlertDialog = builder.create()
//            alertDialog.show()
//        }

    }


}